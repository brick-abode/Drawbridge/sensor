#!/usr/bin/env bash

export CURRENT_PATH=$(pwd)

find_cmd="find"
if [[ "$OSTYPE" == "darwin"* ]]; then
	find_cmd="gfind"
fi

for platformio_path in $($find_cmd . -name 'platformio.ini'); do
	project=${platformio_path::-15}
	echo Building: $project

	platformio ci \
	--project-conf="$project/platformio.ini" \
	--lib="$project" \
	$($find_cmd $project -regex ".*\.\(h\|cpp\|ino\)" -not -path "*/.pio/*")

	# error if any project fails
	if [ $? -ne 0 ]; then
	    exit 1
	fi
done
