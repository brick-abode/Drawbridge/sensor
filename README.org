#+OPTIONS: ^:nil
#+title: Drawbridge Sensors
* Introduction
This repository aims to store all the sensors information utilized in
drawbridge HSM (Hardware Security Module) project. Each sensor behavior, implementation, theoretical explanation and
use cases can be found here.

Also, an interface is presented as a standard solution for modularization of the
sensors implementations.

* Code Standards
We are using uncrustify (C/C++) to help maintain a consistent coding standard.
=Uncrustify-0.70.1_f= is the version utilized in this project. Package managers
such as =zypper= or =apt-get= will provide you developer earlier uncrustify
versions so we recommend [[https://nixos.org/nix/][Nix]] to install the version required.

** Variable and Function Names
For naming functions and variables, we are using the same convention adopted in
=Drawbridge= repository. For consistency, please check it [[https://git.brickabode.com/drawbridge/drawbridge][here]].
** Installing Nix
#+BEGIN_SRC sh
curl https://nixos.org/nix/install | sh
#+END_SRC
** Installing Uncrustify using Nix
All you need is:
#+BEGIN_SRC sh
nix-env -i uncrustify
#+END_SRC
You can check if the version installed is the proper one:
#+BEGIN_SRC sh
uncrustify --version
#+END_SRC
** Running Uncrustify

After installation, you should run in the root of the project the following command:
#+BEGIN_SRC sh
uncrustify -c uncrustify.cfg --replace --no-backup $(find ./ -regex ".*\.\(h\|cpp\|ino\)")
#+END_SRC

The Regular Expression ([[https://en.wikipedia.org/wiki/Regular_expression][Regex]]) passed as
parameter intent to find all files in the project with the extensions
described. The =--replace= command will apply the =uncrustify standard format=
defined on the configuration file =uncrustify.cfg= in all of them.
* Sensors

[[https://git.brickabode.com/drawbridge/drawbridge/blob/master/doc/newspec.org][Drawbridge]] aims to add every manner of intrusion detection to protect the =BCB=
([[https://git.brickabode.com/drawbridge/drawbridge/blob/smallConcreteBlockTest/reports/sensors/ConcreteBlockTest/1st-small-concrete-block-test.org][Big Concrete Box]]). To do so, we have the following sensors, with their
respective reports being tracked in this repository:

 * [[https://git.brickabode.com/drawbridge/drawbridge/blob/master/reports/sensors/temperature/report/temperatureSensors.org][Temperature Sensor]]
 * Water Sensor
 * [[https://git.brickabode.com/drawbridge/drawbridge/blob/report-SNS001/reports/sensors/accelerometers/report/accelerometersReport.org][Accelerometer]]
 * Magnetic Sensor
 * [[https://git.brickabode.com/drawbridge/drawbridge/blob/microphone-sensor-report/reports/sensors/microphone/microphone-report.org][Microphone Sensor]]

All of these sensors will be connected to the [[https://git.brickabode.com/drawbridge/drawbridge/blob/securityEngine/reports/sensors/SecurityEngine/report/SecurityEngineReport.org][Security Engine]] which will
receive and process data from the sensors, activating the Key Destruction Unit (KDU) if any
suspicious activity against the =BCB= is detected.
* Sensor Interface

The sensors Interface is a reliable standard model that all of the
sensors are going to follow.

One of the primary use cases of an interface is to achieve consistency and enforce
some conventions.

All sensors usually work similarly, they can be initialized and read. Admittedly
they also have their differences; each of them has their data types and
implementation definitions according to their software requirements.

The objective of this interface is to achieve the flexibility enough to allow
each sensor work how it was built to, but following a standard with all typical
features that they have.

** Method

We are using a base and abstract class =ISensor= with some mandatory [[https://www.geeksforgeeks.org/pure-virtual-functions-and-abstract-classes/][pure
virtual functions]] that all sensors must inherit from. Each sensor specialization
must provide their own implementation for those functions. We are also using
[[https://www.geeksforgeeks.org/templates-cpp/][templates]] as a way to provide [[https://en.wikipedia.org/wiki/Data_type][data types]] as parameters.

- ISensor class ::

Check the =ISensor= class:

 
#+INCLUDE: "ISensor/lib/ISensor/ISensor.h" src c++

This is the class that all sensor specialization will inherit from.

- ISensor Diagram ::
#+CAPTION: ISensor Diagram
#+NAME: ISensor Diagram
#+ATTR_HTML: :width 80%
#+ATTR_ORG: :width 100
[[./isensor-diagram/diagram.png]]

- The notation is basically a "flexible" [[https://en.wikipedia.org/wiki/Unified_Modeling_Language][Unified Modeling Language (UML)]], which
  contains some formality, but also aims to be easily understandable.
- The software used was [[https://www.visual-paradigm.com][Visual Paradigm]].
- Download the diagram in [[./isensor-diagram/diagram.png][png]] or in [[./isensor-diagram/isensor-diagram.zip][Visual Paradigm files]].

** Use cases
To use any of the sensor implementations in this repository, just include the
library in your own project. If you are using PlatformIO, you just include it in
the default lib folder. Otherwise, you need to follow your project convention
for such folders. Besides the sensor implementations, you can also use the
ISensor interface in your own implementations, for sensors that are not already
in this project according with your software needs.

We recommend the git tool [[https://git-scm.com/book/en/v2/Git-Tools-Submodules][submodule]] as a recommend option to do so.

Since the types required as parameters are generic types, we highly recommend to
use [[http://www.cplusplus.com/doc/tutorial/structures/][data structs]] to store each sensor configuration.

** Code sample

Here is a usage example of =ISensor= class for a basic [[https://create.arduino.cc/projecthub/omer-beden/basic-water-sensor-190c7a][Water Sensor]]:

*** Declaration
=water.h=
#+INCLUDE: "ISensor/lib/Water/water.h" src c++  
*** Definition
=water.cpp=
#+INCLUDE: "ISensor/lib/Water/water.cpp" src c++ 
*** Usage
This is a usage example based on an Arduino project

=main.cpp=
#+BEGIN_SRC cpp
#include "waterSensor.h"

const WATER_PIN {A0};

WATER water(WATER_PIN);

void setup() {
  water.initialize();
}

void loop() {
  int waterRead;
  waterRead = water.read();
}

#+END_SRC
** Conclusion
Besides the solid base structure for different sensors,
this interface will provide to your project readability
and low-coupling which will positively impact future software maintainability.
* Developer Instructions
** commit hooks
   This project uses [[https://pre-commit.com/#plugins][pre-commit]] as a hooks manager. Please install it in your
   system and then in your local repo. The following should work:
   #+BEGIN_SRC sh
     pip install pre-commit
     pre-commit install
     pre-commit run --all-files
   #+END_SRC

   Afterwards, the [[file:.pre-commit-config.yaml::hooks:][repo hooks]] will be run always before a pre-commit.

- Observation :: Make sure you're using pip3 to install pre-commit, as it's
                 incompatible with pip2.

* Creating a project
** Folder schema
Your *examples* folder should follow this folder schema:
#+BEGIN_SRC c++
- examples
  - SensorName
    - main.cpp
    - platformio.ini
#+END_SRC

** Setting platformio.ini
You should properly set the *platformio.ini* in your *examples* folder. The file
below is an example of the temperature sensors project. Check the following
lines:

#+BEGIN_SRC ini
[env]
framework = arduino
lib_extra_dirs = ${sysenv.CURRENT_PATH}/ISensor/lib
lib_deps =
	1	; OneWire
	54	; DallasTemperature
#+END_SRC

- The settings in *[env]* section will be valid for all boards.
  + The lines *framework* and *[[https://docs.platformio.org/en/latest/projectconf/section_env_library.html#lib-extra-dirs][lib_extra_dirs]]* are mandatory for all projects in
    order to make the CI works properly and should look like the example above.
  + If you need to use some library add it on *lib_deps* as shown above. Use the
    ID of platformio library because some libraries may have the same name.
    Anyway, comment the name of the library in the same line. =comments in
    platformio.ini begins with ;=

- To add boards that CI will build, you should add the following lines in your
  platformio.ini file:

- ESP32
#+BEGIN_SRC ini
[env:esp32doit-devkit-v1]
platform = espressif32
board = esp32doit-devkit-v1
#+END_SRC

- Teensy 3.6
#+BEGIN_SRC ini
[env:teensy36]
platform = teensy
board = teensy36
#+END_SRC

- Arduino
#+BEGIN_SRC ini
[env:uno]
platform = atmelavr
board = uno
#+END_SRC

You can add multiple boards on the same project.
* References

- [[https://www.tutorialspoint.com/cplusplus/cpp_interfaces.htm][Interfaces in C++ (Abstract Classes)]]
- [[https://www.geeksforgeeks.org/templates-cpp/][Templates in C++]]
- [[https://www.geeksforgeeks.org/virtual-function-cpp/][Virtual Function in C++]]
- [[https://www.geeksforgeeks.org/pure-virtual-functions-and-abstract-classes/][Pure Virtual Functions and Abstract Classes in C++]]
- [[https://projectdrawbridge.org/wp-content/uploads/2019/01/Drawbridge-White-Paper-Final.pdf][Drawbridge White paper]]
- [[https://git.brickabode.com/drawbridge/drawbridge/blob/master/doc/newspec.org][Drawbridge project specifications]]
