#include "ISensor.h"

class Water : ISensor<int, int> {
public:
  Water(int sensorConfig);
  int data;
  bool initialize();
  int read();
  bool close();
  ~Water() {}
};
