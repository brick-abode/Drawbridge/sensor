#include "water.h"

Water::Water(int sensorConfig): ISensor(sensorConfig) {}

bool Water::initialize() {
  pinMode(config, INPUT);
  return true;
}

int Water::read() {
  data = analogRead(config);
  return data;
}

bool Water::close() {
  return true;
}
