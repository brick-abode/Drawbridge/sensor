#include "ISensor.h"
#include <Arduino.h>
#include <FreeRTOS.h>
#include "driver/i2s.h"
#include "math.h"

#define MAX_ATTEMPTS 20
#define BYTES_INT  4

struct MicConfig {

  int BAUD_RATE;
  i2s_port_t I2S_PORT;

  int SAMPLE_RATE;
  int BUFFERS;
  int SCK_PIN;
  int WS_PIN;
  int SD_PIN;
  short bufferSize;
};

struct Data {
  std::vector<int> audioSamples;
};

class Microphone : public ISensor<Data, MicConfig> {
private:

  int BLOCK_COUNT { 8 };

  const i2s_config_t i2s_config = {
    .mode = i2s_mode_t(I2S_MODE_MASTER | I2S_MODE_RX),
    .sample_rate = config.SAMPLE_RATE,
    .bits_per_sample = I2S_BITS_PER_SAMPLE_32BIT,
    .channel_format = I2S_CHANNEL_FMT_ONLY_LEFT,
    .communication_format = i2s_comm_format_t(
      I2S_COMM_FORMAT_I2S | I2S_COMM_FORMAT_I2S_MSB),
    .intr_alloc_flags = ESP_INTR_FLAG_LEVEL1,
    .dma_buf_count = BLOCK_COUNT,
    .dma_buf_len = config.bufferSize / BLOCK_COUNT
  };

  const i2s_pin_config_t pin_config = {
    .bck_io_num = config.SCK_PIN,
    .ws_io_num = config.WS_PIN,
    .data_out_num = I2S_PIN_NO_CHANGE,
    .data_in_num = config.SD_PIN
  };

  bool begin();
  Data micData;
  bool micInitialized = false;

public:

  Microphone(MicConfig config);
  bool initialize();
  Data read();
  #ifdef TEST
  void generateSin();
  void sendSin();
  #endif
  ~Microphone() {}
};
