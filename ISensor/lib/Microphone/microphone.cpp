#include "microphone.h"

#ifdef TEST
#define BYTES_TO_READ 1024
#define TEST_BUFFER_SIZE BYTES_TO_READ / BYTES_INT
#define SINE_GAIN 22000
#define SINE_SAMPLE_RATE 16000
short sinWave[SINE_SAMPLE_RATE];
int32_t sample[TEST_BUFFER_SIZE] = { 0 };
#endif
#define AUDIO_SAMPLES_GAIN 2
#define ALL_ZEROS 0

Microphone::Microphone(MicConfig micConfig): ISensor(micConfig) {
  this->config = micConfig;
}

#ifdef TEST

void Microphone::generateSin() {
  float freq = 2000.0f;
  int seconds = 1;
  unsigned sample_rate = SINE_SAMPLE_RATE;
  size_t buf_size = seconds * sample_rate;
  for (int i = 0; i < buf_size; ++i) {
    sinWave[i] =
      int16_t(GAIN * sin((2.f * float(M_PI) * freq) / sample_rate * i));
  }
}

void Microphone::sendSin() {
  for (int i = 0; i < SINE_SAMPLE_RATE; i++) {
    Serial.println(sinWave[i]);
  }
}

#endif

bool Microphone::initialize() {

  int counter = 0;

  #ifdef TEST
  generateSin();
  #endif
  while (counter < MAX_ATTEMPTS && !micInitialized) {

    micInitialized = begin();

    /* This Serial print sequence aims to facilitate debug mode. */
    #ifdef TEST
    if (micInitialized == true) {
      Serial.print("\n");
      Serial.println("Initialized successfully Microphone Sensor...");
    } else {

      Serial.printf("Counter >>> %d ", counter);
      Serial.print("\n");
      Serial.print("Response is ");
      Serial.printf(micInitialized ? "true" : "false");
      Serial.print("\n");
      Serial.println("Trying ... ");
    }
    #endif
    ++counter;
  }
  return micInitialized;
}

bool Microphone::begin() {

  esp_err_t esp_err;
  bool success = false;

  esp_err = i2s_driver_install(config.I2S_PORT, &i2s_config, 0, NULL);

  if (esp_err != ESP_OK) {
    ESP.restart();
  }

  esp_err = i2s_set_pin(config.I2S_PORT, &pin_config);

  if (esp_err != ESP_OK) {
    ESP.restart();
  } else {
    success = true;
  }

  return success;
}

Data Microphone::read() {
  int audioSamples[config.bufferSize];
  short BYTES_TO_READ = config.bufferSize * BYTES_INT;

  size_t bytes_read;
  micData.audioSamples = std::vector<int>(config.bufferSize, ALL_ZEROS);

  i2s_read(config.I2S_PORT,
           (char *) &audioSamples,
           BYTES_TO_READ,
           &bytes_read,
           portMAX_DELAY);

  for (int i = 0; i < micData.audioSamples.size(); i++) {
    micData.audioSamples[i] = (AUDIO_SAMPLES_GAIN * audioSamples[i]);
  }

  return micData;
}
