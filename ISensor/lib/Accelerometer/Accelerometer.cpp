#include "Accelerometer.h"

Accelerometer::Accelerometer(GYSensorConfig SensorConfig): ISensor(SensorConfig)
{}

bool Accelerometer::initialize() {
  Wire.begin();
  Wire.beginTransmission(config.MPU_ADDR);
  WakeUpGySensor();
  int Response = EndTransmission();
  return (Response == SUCCESS);
}

GYSensorData Accelerometer::read() {
  GYSensorData Data;

  if (config.GetAccelData) {
    Triplet<int16_t> rawData = readRawData(ACCELEROMETER);
    Data.Accel = convertToAcceleration(rawData);
  }

  if (config.GetTemperatureData) {
    Triplet<int16_t> rawData = readRawData(TEMPERATURE);
    Data.temperature = convertToTemperature(rawData);
  }
  if (config.GetGyroData) {
    Triplet<int16_t> rawData = readRawData(GYROSCOPE);
    Data.Gyroscope = convertToGyroscope(rawData);
  }

  Data.success = wasReadSuccessful(Data);
  return Data;
}

bool Accelerometer::wasReadSuccessful(GYSensorData CheckData) {
  bool check = ((CheckData.Accel.x >= MIN_ACCELERATION_FOR_CONNECTED_SENSOR) ||
                CheckData.Accel.y >= MIN_ACCELERATION_FOR_CONNECTED_SENSOR
                || CheckData.Accel.z >= MIN_ACCELERATION_FOR_CONNECTED_SENSOR);
  return check;
}

Triplet<int16_t> Accelerometer::readRawData(Sensor sensor) {
  int16_t raw1 = 0, raw2 = 0, raw3 = 0;
  Wire.beginTransmission(config.MPU_ADDR);
  RegisterInfo regInfo = getRegisterInfo(sensor);
  Wire.write(regInfo.RegisterAddress);
  KeepTransmission();
  Wire.requestFrom(config.MPU_ADDR, regInfo.quantity, true);
  raw1 = Wire.read() << BIT_SHIFT | Wire.read();
  if (sensor != TEMPERATURE) {
    raw2 = Wire.read() << BIT_SHIFT | Wire.read();
    raw3 = Wire.read() << BIT_SHIFT | Wire.read();
  }
  Triplet<int16_t> rawData = { raw1, raw2, raw3 };
  return rawData;
}

float Accelerometer::convertToTemperature(Triplet<int16_t> rawData) {
  const float TEMPERATURE_DIVISOR = 340.00;
  const float TEMPERATURE_OFFSET = 36.53;
  return rawData.x / TEMPERATURE_DIVISOR + TEMPERATURE_OFFSET;
}

Triplet<float> Accelerometer::convertToAcceleration(Triplet<int16_t> rawData) {
  const float ACCEL_DIVISOR = 16384.0;
  Triplet<float> acceleration = {
    abs(rawData.x / ACCEL_DIVISOR),
    abs(rawData.y / ACCEL_DIVISOR),
    abs(rawData.z / ACCEL_DIVISOR)
  };
  return acceleration;
}

Triplet<float> Accelerometer::convertToGyroscope(Triplet<int16_t> rawData) {
  const float GYRO_DIVISOR = 131.0;
  Triplet<float> gyroscope = {
    abs(rawData.x / GYRO_DIVISOR),
    abs(rawData.y / GYRO_DIVISOR),
    abs(rawData.z / GYRO_DIVISOR)
  };
  return gyroscope;
}

RegisterInfo Accelerometer::getRegisterInfo(Sensor sensor) {
  RegisterInfo regInfo;
  switch (sensor) {
    case ACCELEROMETER:
      regInfo.RegisterAddress = ACCEL_REG_ADDR;
      regInfo.quantity = 6;
      break;
    case TEMPERATURE:
      regInfo.RegisterAddress = TEMPERATURE_REG_ADDR;
      regInfo.quantity = 2;
      break;
    case GYROSCOPE:
      regInfo.RegisterAddress = GYRO_REG_ADDR;
      regInfo.quantity = 6;
      break;
  }
  return regInfo;
}
