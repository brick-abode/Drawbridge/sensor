#include "ISensor.h"
#include "Wire.h"
#define STOP_TRANSMISSION true
#define KEEP_TRANSMISSION false
#define BIT_SHIFT 8
#define MPU_WAKEUP_VALUE 0
#define SUCCESS 0
#define QUANTITY 6

template<typename T>
struct Triplet {
  T x;
  T y;
  T z;
};

struct RegisterInfo {
  int RegisterAddress;
  int quantity;
};

struct GYSensorConfig {
  int MPU_ADDR;
  bool GetAccelData, GetGyroData, GetTemperatureData;
};

struct GYSensorData {
  Triplet<float> Accel;
  Triplet<float> Gyroscope;
  float temperature;
  bool success = false;
};

enum Sensor {
  ACCELEROMETER,
  TEMPERATURE,
  GYROSCOPE
};

class Accelerometer : public ISensor<GYSensorData, GYSensorConfig> {
  const int POWER_MANAGMENT_REGISTER_1 = 0x6B;
  const int ACCEL_REG_ADDR = 0x3B;
  const int GYRO_REG_ADDR = 0x43;
  const int TEMPERATURE_REG_ADDR = 0x41;
  const float MIN_ACCELERATION_FOR_CONNECTED_SENSOR = 0.001;

  bool wasReadSuccessful(GYSensorData AccelData);
  Triplet<int16_t> readRawData(Sensor sensor);
  float convertToTemperature(Triplet<int16_t> rawData);
  Triplet<float> convertToAcceleration(Triplet<int16_t> rawData);
  Triplet<float> convertToGyroscope(Triplet<int16_t> rawData);
  RegisterInfo getRegisterInfo(Sensor sensor);
  void WakeUpGySensor() {
    Wire.write(POWER_MANAGMENT_REGISTER_1);
    Wire.write(MPU_WAKEUP_VALUE);
  }

  int KeepTransmission() {
    int Response = Wire.endTransmission(false);
    return Response;
  }

  int EndTransmission() {
    int Response = Wire.endTransmission(true);
    return Response;
  }

public:
  Accelerometer(GYSensorConfig SensorConfig);
  bool initialize();
  GYSensorData read();
  ~Accelerometer() {}
};
