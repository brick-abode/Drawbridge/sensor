#include "temperature.h"

Temperature::Temperature(sensorConfig sensorConfig): ISensor(sensorConfig) {}

bool Temperature::initialize() {
  oneWire = new OneWire(config.pin);
  sensors = new DallasTemperature(oneWire);
  sensors->begin();

  int searchSensors = 0;
  while (oneWire->search(Addr[searchSensors])) {
    searchSensors++;
  }
  amountOfSensors = searchSensors;

  return (amountOfSensors == config.expectedAmountOfSensors);
}

double * Temperature::read() {
  sensors->requestTemperatures();
  double * data = new double[amountOfSensors];

  for (int sensorNumber = 0; sensorNumber < amountOfSensors; sensorNumber++) {
    double sensorValue = sensors->getTempC(Addr[sensorNumber]);
    data[sensorNumber] = sensorValue;
  }

  return data;
}

template<typename T>
void Temperature::deletePtr(T* ptr) {
  if (!ptr) {
    delete ptr;
    ptr = nullptr;
  }
}

bool Temperature::close() {
  deletePtr(oneWire);
  deletePtr(sensors);

  amountOfSensors = 0;

  return true;
}

int Temperature::getAmountOfSensors() {
  return amountOfSensors;
}
