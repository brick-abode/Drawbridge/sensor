#pragma once
#include <OneWire.h>
#include <DallasTemperature.h>
#include "ISensor.h"

#define BYTE_ADDRESS_SIZE 8
#define MAX_TEMP_SENSORS 20

struct sensorConfig {
  int pin;
  int expectedAmountOfSensors;
};

class Temperature : ISensor<double*, sensorConfig> {
private:
  int amountOfSensors = 0;
  DallasTemperature * sensors = nullptr;
  OneWire * oneWire = nullptr;
  byte Addr[MAX_TEMP_SENSORS][BYTE_ADDRESS_SIZE];
  double data;
  template<typename T> void deletePtr(T* ptr);

public:
  Temperature(sensorConfig sensorConfig);
  bool initialize();
  double * read();
  bool close();
  ~Temperature() {
    close();
  }

  int getAmountOfSensors();
};
