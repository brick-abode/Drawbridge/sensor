#pragma once
#include <Arduino.h>
template<class Data, class Config>
class ISensor {
public:
  Config config;
  ISensor(Config sensorConfig) {
    config = sensorConfig;
  }

  virtual bool initialize() = 0;
  virtual Data read() = 0;
  virtual ~ISensor() {
    Serial.println("Destroyed sensor object");
  }
};
