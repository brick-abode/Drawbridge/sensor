#include <Arduino.h>
#include "temperature.h"

#define BAUD_RATE 115200
#define PIN 5
#define EXPECTED_AMOUNT_OF_SENSORS 2
#define INITIALIZE_DELAY 1000

sensorConfig config =
{ PIN, EXPECTED_AMOUNT_OF_SENSORS };

Temperature temperature(config);

void setup() {
  Serial.begin(BAUD_RATE);
  Serial.println("Initializing...");
  while (!temperature.initialize()) {
    Serial.println("Error. Check the number of sensors or reset the device!");
    delay(INITIALIZE_DELAY);
  }
  int amountOfSensors = temperature.getAmountOfSensors();
  Serial.println();
  Serial.print("Sensors found: ");
  Serial.println(amountOfSensors);
}

void loop() {
  int amountOfSensors = temperature.getAmountOfSensors();
  double * temperatures = temperature.read();

  for (int i = 0; i < amountOfSensors; i++) {
    Serial.print("Sensor[");
    Serial.print(i);
    Serial.print("]: ");
    Serial.print(temperatures[i]);
    Serial.println(" C");
  }
}
