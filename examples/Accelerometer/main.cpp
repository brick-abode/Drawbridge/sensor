#include <Arduino.h>
#include "Accelerometer.h"
#define BAUD_RATE 115200
GYSensorConfig configSens1 = { 0x68, true, true, false };
Accelerometer Accel1(configSens1);

void showData(Triplet<float> SensorData) {
  Serial.print("X= ");
  Serial.println(SensorData.x);
  Serial.print("Y= ");
  Serial.println(SensorData.y);
  Serial.print("Z= ");
  Serial.println(SensorData.z);
}

void setup() {
  Serial.begin(BAUD_RATE);
  bool response = Accel1.initialize();
  if (!response) {
    Serial.println("Failure to initialize accel sensor");
    Accel1.initialize();
  } else {
    Serial.println("Sensor Initialized");
  }
}

void loop() {

  struct GYSensorData ReadData;
  ReadData = Accel1.read();
  if (!ReadData.success) {
    Serial.println("ERROR: Bad contact or pin config problem");
    Serial.println(
      "TRYING TO AUTO-RESET, please RESET THE ESP and check the pinout ");
    Accel1.initialize();
  }

  Serial.println("Accelerometer Data:");
  showData(ReadData.Accel);

  Serial.println("Gyro Data:");
  showData(ReadData.Gyroscope);
}
