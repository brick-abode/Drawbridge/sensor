#include <Arduino.h>
#include "ISensor.h"
#include "microphone.h"

std::vector<int> audioSample;
MicConfig micConfig {
  912000,                                         // BAUD_RATE
  I2S_NUM_0,                                         // i2s_port_t
  8000,                                         // SAMPLE_RATE
  8,                                        // BUFFERS
  14,                                        // SCK: Serial-Data clock for I2S
  15,                                        // WS: Serial-Data-Word select for
                                             // I2S
  32,                                        // SD: Serial-Data output
  256                                      // samples buffer size
};

Microphone micSensor(micConfig);

void setup() {

  Serial.begin(micConfig.BAUD_RATE);
  micSensor.initialize();
}

void loop() {

  Data micData;
  micData = micSensor.read();

  for (size_t i = 0; i < micData.audioSamples.size(); i++) {
    Serial.println(micData.audioSamples[i]);
  }
}
