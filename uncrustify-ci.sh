#!/usr/bin/env bash

set -ex

uncrustify --version

uncrustify -c uncrustify.cfg --check $(find ./ -regex ".*\.\(h\|cpp\|ino\)")
